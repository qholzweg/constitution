import 'package:flutter/material.dart';
import 'package:tinycolor/tinycolor.dart';

ThemeData buildTheme() {
  final ThemeData baseLight = ThemeData.light();
  return baseLight.copyWith(
    visualDensity: VisualDensity.adaptivePlatformDensity,
    primaryColor: primary[700],
    backgroundColor: Colors.white,
    colorScheme: ColorScheme.light(
      primary: primary[700],
      secondary: secondary,
    ),

    textTheme: _buildTtTextTheme(baseLight.textTheme),
    primaryTextTheme: _buildTtTextTheme(baseLight.primaryTextTheme),
    accentTextTheme: _buildTtTextTheme(baseLight.accentTextTheme),

    primaryIconTheme: baseLight.iconTheme.copyWith(
      color: Colors.white,
    ),

    hintColor: Colors.grey[400],
    dividerColor: Colors.grey[200],

    /// App Bar decoration
    appBarTheme: AppBarTheme(
      color: Colors.white,
      brightness: Brightness.light,
      elevation: 1,
      textTheme: TextTheme(
          headline6: TextStyle(
        color: Colors.black,
        fontSize: 20,
        fontFamily: 'Montserrat',
      )),
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
    ),

    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(25)
      ),
      labelStyle: TextStyle(fontSize: 16.0),
      contentPadding: EdgeInsets.symmetric( vertical: 8, horizontal: 16)
    ),

    buttonTheme: baseLight.buttonTheme.copyWith(
      buttonColor: primary[700],
      textTheme: ButtonTextTheme.primary,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    ),

    accentIconTheme: baseLight.iconTheme.copyWith(
      color: Colors.white,
    ),
  );
}

TextTheme _buildTtTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline5: base.headline5.copyWith(),
        headline6: base.headline6.copyWith(fontSize: 18.0, height: 1.2),
        subtitle2: base.subtitle2.copyWith(height: 1),
        caption: base.caption.copyWith(
          fontSize: 16.0,
        ),
      )
      .apply(fontFamily: 'Montserrat');
}

/// COLORS -----------------

final int _blue = 0xFF2A7097;
final int _yellow = 0xFFEBC53F;
final int _red = 0xFFED1C24;
final int _green = 0xFF35765E;

MaterialColor primary = colorConvert(_blue);
MaterialColor secondary = colorConvert(_red);

MaterialColor blue = colorConvert(_blue);
MaterialColor yellow = colorConvert(_yellow);
MaterialColor red = colorConvert(_red);
MaterialColor green = colorConvert(_green);

MaterialColor colorConvert(int color) {
  return MaterialColor(color, <int, Color>{
    900: TinyColor(Color(color)).darken(20).color,
    800: TinyColor(Color(color)).darken().color,
    700: Color(color),
    600: TinyColor(Color(color)).lighten().color,
    500: TinyColor(Color(color)).lighten(20).color,
    400: TinyColor(Color(color)).lighten(30).color,
    300: TinyColor(Color(color)).lighten(40).color,
    200: TinyColor(Color(color)).lighten(50).color,
    100: TinyColor(Color(color)).lighten(60).color,
  });
}

import 'package:constitution/datamodels/constitution_model.dart';
import 'package:constitution/datamodels/question.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class SurveyService {
  List<Question> _survey;
  List<Question> get survey => _survey;
  SurveyService(){
    _survey = constitutionModel.map(
            (e) => Question.fromMap(e)
    ).toList();
  }

  String getTitle(int index) => survey[index].title;
  String getDescription(int index) => survey[index].description;
  String getLaw(int index) => survey[index].law;
  String getAuthor(int index) => survey[index].author;
  String getCurrentLaw(int index) => survey[index].currentLaw;
}
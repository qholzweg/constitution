import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

@injectable
class Api{
  final Firestore _db = Firestore.instance;
  CollectionReference ref;

  Api( String path ) {
    ref = _db.collection(path);
  }

  Future<QuerySnapshot> getDataCollection() {
    return ref.getDocuments() ;
  }
  Stream<QuerySnapshot> streamDataCollection() {
    return ref.snapshots() ;
  }
  Future<DocumentSnapshot> getDocumentById(String id) {
    return ref.document(id).get();
  }
  Future<QuerySnapshot> getDocumentByParam(String name, dynamic value) {
    return ref.where(name, isEqualTo: value).snapshots().firstWhere((element) => true);
  }

  Future<void> removeDocument(String id){
    return ref.document(id).delete();
  }
  Future<DocumentReference> addDocument(Map data) {
    return ref.add(data);
  }
  void setDocument(String id, Map data, {bool update = false}) {
    if (update) data.putIfAbsent('date_modified', FieldValue.serverTimestamp);
    else data.putIfAbsent('date_created', FieldValue.serverTimestamp);
    DocumentReference _doc = ref.document(id);
    var _result = _doc.setData(data);
    print(_result);
  }
  Future<void> updateDocument(Map data , String id) {
    return ref.document(id).updateData(data) ;
  }
  Future<void> runTransaction({String id, Function onSuccess, Function onFail}) {
    final DocumentReference postRef = ref.document(id);
    return _db.runTransaction((Transaction tx) async {
      DocumentSnapshot voteSnapshot = await tx.get(postRef).catchError((onError) => onFail());
      if (voteSnapshot.exists) {
        if (onSuccess != null) await tx.update(postRef, onSuccess(voteSnapshot.data)).catchError((onError) => onFail());
      } else {
        if (onFail != null) onFail();
      }
    });
  }


}
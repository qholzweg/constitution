import 'dart:io';

import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:constitution/app/common_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';

enum SignInMethod { anonymous, google, apple, facebook }

class AuthResultUserId {
  final bool success;
  final String userId;
  final String error;

  AuthResultUserId({this.success, this.userId, this.error});
}

@lazySingleton
class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  AuthResultUserId authResultUserId;

  Future<FirebaseUser> getUser() async => await _auth.currentUser();

  Future<void> signOut() async {
    authResultUserId = null;
    return await _auth.signOut();
  }

  Future<AuthResultUserId> signIn(SignInMethod signInMethod) async {
    AuthCredential credential;
    FirebaseUser user;
    authResultUserId = null;
    try {
      if (signInMethod == SignInMethod.anonymous)
        user = await signInAnonymously();
      else {
        switch (signInMethod) {
          case SignInMethod.apple:
            final AuthorizationResult result =
                await AppleSignIn.performRequests([
              AppleIdRequest(requestedScopes: [Scope.email])
            ]);
            if (result.status == AuthorizationStatus.authorized) {
              final AppleIdCredential appleIdCredential = result.credential;
              OAuthProvider oAuthProvider =
                  new OAuthProvider(providerId: "apple.com");
              credential = oAuthProvider.getCredential(
                idToken: String.fromCharCodes(appleIdCredential.identityToken),
                accessToken:
                    String.fromCharCodes(appleIdCredential.authorizationCode),
              );
            } else
              authResultUserId = AuthResultUserId(
                  success: false, error: result.status.toString());
            break;

          case SignInMethod.google:
            ///check for ios 10 and 11
            GoogleSignIn _googleSignIn;
            if (await isOldIos()) {
              _googleSignIn = GoogleSignIn(
                scopes: [
                  'email',
                  'https://www.googleapis.com/auth/contacts.readonly'
                ],
                hostedDomain: "",
                clientId: "",
              );
            } else
              _googleSignIn = GoogleSignIn();
            final GoogleSignInAccount googleUser =
                await _googleSignIn.signIn().catchError((onError) {
              authResultUserId =
                  AuthResultUserId(success: false, error: onError.toString());
              return false;
            });
            if (googleUser != null) {
              final GoogleSignInAuthentication googleAuth =
                  await googleUser.authentication;
              credential = GoogleAuthProvider.getCredential(
                accessToken: googleAuth.accessToken,
                idToken: googleAuth.idToken,
              );
            } else
              authResultUserId = AuthResultUserId(success: false);
            break;

          case SignInMethod.facebook:
            FacebookLoginResult facebookLoginResult = await _handleFBSignIn();
            if (facebookLoginResult.status == FacebookLoginStatus.loggedIn) {
              final accessToken = facebookLoginResult.accessToken.token;
              credential =
                  FacebookAuthProvider.getCredential(accessToken: accessToken);
            } else
              authResultUserId = AuthResultUserId(
                  success: false, error: FacebookLoginStatus.error.toString());
            break;

          default:
            authResultUserId = AuthResultUserId(success: false);
            break;
        }

        user = await signInWithCredentials(credential).catchError((onError) =>
            authResultUserId =
                AuthResultUserId(success: false, error: onError.toString()));
      }
    } catch (error) {
      authResultUserId =
          AuthResultUserId(success: false, error: error.toString());
    }

    if (authResultUserId != null && !authResultUserId.success)
      return authResultUserId;

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
    if (user != null) {
      return AuthResultUserId(success: true, userId: user.uid);
    } else {
      return AuthResultUserId(success: false, userId: user.uid);
    }
  }

  Future<FacebookLoginResult> _handleFBSignIn() async {
    FacebookLogin facebookLogin = FacebookLogin();
    FacebookLoginResult facebookLoginResult =
        await facebookLogin.logIn(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.cancelledByUser:
        print("Cancelled");
        break;
      case FacebookLoginStatus.error:
        print("error");
        break;
      case FacebookLoginStatus.loggedIn:
        print("Logged In");
        break;
    }
    return facebookLoginResult;
  }

  Future<FirebaseUser> signInWithCredentials(credential) async {
    FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
    assert(user.email != null);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);
    return user;
  }

  Future<FirebaseUser> signInAnonymously() async {
    final FirebaseUser user = (await _auth.signInAnonymously()).user;
    assert(user != null);
    assert(user.isAnonymous);
    assert(!user.isEmailVerified);
    assert(await user.getIdToken() != null);
    if (Platform.isIOS) {
      // Anonymous auth doesn't show up as a provider on iOS
      assert(user.providerData.isEmpty);
    } else if (Platform.isAndroid) {
      // Anonymous auth does show up as a provider on Android
      assert(user.providerData.length == 1);
      assert(user.providerData[0].providerId == 'firebase');
      assert(user.providerData[0].uid != null);
      assert(user.providerData[0].displayName == null);
      assert(user.providerData[0].photoUrl == null);
      assert(user.providerData[0].email == null);
    }

    return user;
  }
}

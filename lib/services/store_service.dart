import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:constitution/app/locator.dart';
import 'package:constitution/datamodels/survey_result.dart';
import 'package:constitution/services/votes_service.dart';
import 'package:injectable/injectable.dart';

import 'api.dart';

@lazySingleton
class StoreService {
  Api _votesApi = Api('votes');
  Api _voteResultsApi = Api('vote_results');

  SurveyResult _result;
  SurveyResult get result => _result;

  Future<SurveyResult> getResultFuture() async => _result != null
      ? Future<SurveyResult>.value(_result)
      : getSurveyResult()
          .catchError((onError) => throw Exception('Не могу загрузить'));

  VotesService _votesService = locator<VotesService>();

  Future removeVote(String id) async {
    await _votesApi.removeDocument(id);
    return;
  }

  Future<DocumentSnapshot> getVote(String voteId) async => await _votesApi.getDocumentById(voteId);

  Future updateVote(data, String id) async {
    await _votesApi.updateDocument(data.toJson(), id);
    return;
  }

  Future addVote(data) async {
    var result = await _votesApi.addDocument(data);
    return result;
  }

  Future setVote(String userId, Map<String, dynamic> data, {bool update = false}) async {
    var result = await _votesApi.setDocument(userId, data, update: update);
    return result;
  }

  Future<void> clearResult() async {
    _voteResultsApi.updateDocument(
        SurveyResult.empty().toMap(), 'constitution');
  }

  Future<SurveyResult> getSurveyResult() async {
    DocumentSnapshot _doc =
        await _voteResultsApi.getDocumentById('constitution');
    _result = SurveyResult.fromMap(_doc.data);
    return _result;
  }

  Future addVoteResult() async {
    return await _voteResultsApi.runTransaction(
        id: 'constitution',
        onSuccess: (snapshotData) {
          SurveyResult _oldResult = SurveyResult.fromMap(snapshotData);
          _result = _oldResult + _votesService.surveyResult;
          return _result.toMap();
        },
      onFail: () => throw('Ошибка передачи данных')
    );
  }

  Future removeVoteResult(SurveyResult resultToRemove) async {
    return await _voteResultsApi.runTransaction(
        id: 'constitution',
        onSuccess: (snapshotData) {
          SurveyResult _oldResult = SurveyResult.fromMap(snapshotData);
          _result = _oldResult - resultToRemove;
          return _result.toMap();
        },
        onFail: () => throw('Ошибка передачи данных')
    );
  }
}

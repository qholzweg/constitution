import 'package:constitution/datamodels/survey_result.dart';
import 'package:constitution/datamodels/vote_result.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class VotesService {
  int _currentIndex = 0;
  int get currentIndex => _currentIndex;

  SurveyResult _surveyResult = SurveyResult.populated();
  SurveyResult get surveyResult => _surveyResult;

  VoteResultType _result;
  VoteResultType get result => getResult();

  double _approvalIndex;
  double get approvalIndex => getApprovalIndex();

  VoteResultType getCurrentVoteResult() =>
      _surveyResult.voteResult(_currentIndex);

  int getCurrentWeight() => _surveyResult.voteWeight(_currentIndex);

  void updateVote({VoteResultType result, int weight}) {
    _surveyResult.votes[_currentIndex]
        .setValuesFromResultType(result: result, weightValue: weight);
  }

  VoteResultType getResult() {
    _result = _surveyResult.getMyResultType();
    return _result;
  }

  double getApprovalIndex() {
    _approvalIndex = _surveyResult.getMyIndex();
    return _approvalIndex;
  }

  bool next(VoteResultType voteResult) {
    updateVote(result: voteResult);
    if (_currentIndex < _surveyResult.voteCount - 1) {
      _currentIndex++;
      return true;
    }
    return false;
  }

  bool previous() {
    if (_currentIndex > 0) {
      _currentIndex--;
      return true;
    }
    return false;
  }

  void clear() {
    _currentIndex = 0;
    _result = null;
    _approvalIndex = null;
    _surveyResult = SurveyResult.populated();
  }
}

import 'package:constitution/ui/components/vote_bubble_template.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'vote_bubble_viewmodel.dart';

class VoteBubble extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<VoteBubbleViewModel>.reactive(
      builder: (context, model, child) =>
          VoteBubbleTemplate(
            title: model.title,
            description: model.description,
            law: model.law,
            author: model.author,
            onDetailsPress: model.navigateToDetails,
            footer: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('Важность', style: TextStyle(fontSize: 16),),
                    Container(
                      width: 32,
                      height: 32,
                      child: IconButton(
                        icon: Icon(Icons.help, color: Colors.black.withOpacity(.5),),
                        iconSize: 28,
                        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                        onPressed: model.weightHelpDialog,
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    Slider.adaptive(
                      value: model.weight.toDouble() ?? 5.0,
                      onChanged: (newWeight) =>
                          model.onWeightChange(newWeight.toInt()),
                      min: 0,
                      max: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [Text('Не важно'), Text('Важно')],
                    )
                  ],
                )
              ],
            ),
          ),
      viewModelBuilder: () => VoteBubbleViewModel(),
    );
  }
}


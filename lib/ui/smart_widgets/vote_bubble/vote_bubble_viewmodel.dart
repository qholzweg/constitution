import 'package:constitution/app/locator.dart';
import 'package:constitution/services/survey_service.dart';
import 'package:constitution/services/votes_service.dart';
import 'package:constitution/ui/components/dialog_service.dart';
import 'package:constitution/ui/views/vote_details/vote_details_view.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class VoteBubbleViewModel extends BaseViewModel {
  final SurveyService _surveyService = locator<SurveyService>();
  final VotesService _votesService = locator<VotesService>();
  final DialogueService _dialogueService = locator<DialogueService>();
  final NavigationService _navigationService = locator<NavigationService>();
  String get title => _surveyService.getTitle(currentIndex);
  String get description => _surveyService.getDescription(currentIndex);
  String get law => _surveyService.getLaw(currentIndex);
  String get author => _surveyService.getAuthor(currentIndex);
  int get currentIndex => _votesService.currentIndex;
  int get weight => _votesService.getCurrentWeight();
  bool get needDialog =>
      (description != null && description.isNotEmpty)
          || (law != null && law.isNotEmpty);

  Future<void> weightHelpDialog () async {
    await _dialogueService.showDialog(
      title: 'Важность поправки',
      description: 'Сдвиньте бегунок вправо, если считаете поправку важной и\u00A0влево, если\u00A0нет. Важность влияет на\u00A0финальный результат, но не\u00A0зависит от\u00A0того, как\u00A0вы относитесь к\u00A0поправке. Например, человек может считать поправку очень важной и\u00A0быть против нее: то\u00A0есть ему очень важно, чтобы такой поправки не\u00A0было.',
      buttonTitle: 'Понятно'
    );
  }

  Future<void> navigateToDetails() async => await _navigationService.navigateWithTransition(VoteDetailsView(), transition: NavigationTransition.Cupertino);

  void onWeightChange (newWeight) {
    _votesService.updateVote(weight: newWeight);
    notifyListeners();
  }
}
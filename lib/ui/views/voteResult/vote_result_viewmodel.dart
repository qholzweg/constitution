import 'package:constitution/app/common_functions.dart';
import 'package:constitution/app/locator.dart';
import 'package:constitution/app/router.gr.dart';
import 'package:constitution/datamodels/survey_result.dart';
import 'package:constitution/datamodels/vote_result.dart';
import 'package:constitution/services/auth_service.dart';
import 'package:constitution/services/votes_service.dart';
import 'package:constitution/services/store_service.dart';
import 'package:constitution/ui/components/dialog_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:constitution/app/constants.dart' as Constants;

class VoteResultViewModel extends FutureViewModel<bool> {
  AuthService _authService = locator<AuthService>();
  VotesService _votesService = locator<VotesService>();
  StoreService _storeService = locator<StoreService>();
  DialogueService _dialogueService = locator<DialogueService>();
  String title = 'Результат';
  String indexText = 'Индекс вашего отношения:';
  String finish = 'Закончить';
  bool isSending = false;
  String connectionError;

  VoteResultType get result => _votesService.result;

  double get resultIndex => _votesService.approvalIndex;

  Future<bool> initialData() async  {
    if (await isOldIos()) connectionError = 'Отправка результатов может вызывать ошибки при нестабильном соединении. Извините за возможные неудобства.';
    return await _authService.getUser() != null;
  }

  final NavigationService _navigationService = locator<NavigationService>();

  String getShareText() {
    String _text;
    switch (result) {
      case VoteResultType.pro:
        _text = 'Я ЗА поправки в Конституцию! Одобряю на ${(resultIndex * 100).toInt().toString()}%. ';
        break;
      case VoteResultType.neutral:
        _text = 'Я нейтрально отношусь к поправкам в Конституцию. ';
        break;
      case VoteResultType.con:
        _text =
            'Я ПРОТИВ поправок в Конституцию! Не одобряю на ${(resultIndex * 100).abs().toInt().toString()}%. ';
        break;
    }
    _text += 'Пройди тест и посмотри, как относятся к поправкам в приложении: ${Constants.LINK_TO_APP}';
    return _text;
  }

  void share(BuildContext context) {
    final RenderBox box = context.findRenderObject();
    Share.share(getShareText(),
            subject: 'Изменения в Конституцию РФ',
            sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size)
        .catchError((onError) async => await _dialogueService.showDialog(
            title: 'Что-то пошло не так',
            description: 'Не могу найти в системе инструменты шеринга',
            buttonTitle: 'Понятно'));
  }

  Future securityInfo () async => await _dialogueService.showDialog(
    title: 'О безопасности',
    description: 'Если вы \u00A0хотите, чтобы ваш результат был учтен в \u00A0статистике, нажмите кнопку «Анонимно опубликовать». Чтобы избежать повторной отправки результатов недобросовестными пользователями, мы \u00A0попросим вас войти через одну из \u00A0платформ идентификации на \u00A0ваш выбор, однако, мы \u00A0не \u00A0будем запрашивать ваши имя, фамилию и \u00A0другие личные данные, кроме email. Данные будут переданы на \u00A0сервер в \u00A0зашифрованном виде, после чего стерты с \u00A0вашего устройства. Если хотите, вы \u00A0можете пройти опрос еще раз, в \u00A0таком случае ваш новый результат будет отражен в \u00A0статистике. Кроме того, вы \u00A0можете поделиться результатом с \u00A0друзьями через ваш мессенджер, нажав кнопку «Поделиться» вверху.',
    buttonTitle: 'Понятно'
  );

  Future navigateToSignIn() async {
    if (!await checkConnection()) return null;
    AuthResultUserId _result =
        await _navigationService.navigateTo(Routes.signInViewViewRoute);
    if (_result != null &&_result.success) publish(userId: _result.userId);
  }

  Future updateDialog() async {
    if (!await checkConnection()) return null;
    DialogueResponse _dialogResponse = await _dialogueService.showDialog(
        title: 'Обновить ваши результаты?',
        description:
            'Вы уже опубликовали результаты опроса под этим аккаунтом. Если вы нажмете "Продолжить" результаты вашего опроса будут обновлены. Если вы хотите учесть результаты голосования другого человека, нажмите "Сменить пользователя" и войдите под новой учетной записью.',
        cancelTitle: 'Сменить пользователя',
        buttonTitle: 'Продолжить');

    if (_dialogResponse.confirmed)
      update();
    else {
      await _authService.signOut();
      navigateToSignIn();
    }
  }

  void onError (error) {
    connectionError = error;
    notifyListeners();
  }

  Future update() async {
    if (!await checkConnection()) return null;
    isSending = true;
    notifyListeners();
    FirebaseUser _user = await _authService.getUser();
    if (_user != null) {
      String userId = _user.uid;
      SurveyResult _resultToRemove = await _storeService
          .getVote(userId)
          .then(
              (value) {
                return SurveyResult.fromMap(value.data);});
      await _storeService.removeVoteResult(_resultToRemove).catchError((onError) => onError(onError));
      await _storeService.setVote(userId, _votesService.surveyResult.toMap(), update: true);
      await _storeService.addVoteResult().catchError((onError) => onError(onError));
      _votesService.clear();
      isSending = false;
      notifyListeners();
      await _navigationService.navigateTo(Routes.surveyStatsViewRoute, arguments: SurveyStatsViewArguments(resultPublished: true));
    }
  }

  Future publish({String userId}) async {
    if (!await checkConnection()) return null;
    isSending = true;
    notifyListeners();
    await _storeService.setVote(userId, _votesService.surveyResult.toMap());
    await _storeService.addVoteResult();
    _votesService.clear();
    isSending = false;
    notifyListeners();
    await _navigationService.navigateTo(Routes.surveyStatsViewRoute, arguments: SurveyStatsViewArguments(resultPublished: true));
  }

  @override
  Future<bool> futureToRun() => initialData();
}

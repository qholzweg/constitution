import 'package:constitution/app/common_functions.dart';
import 'package:constitution/theme.dart';
import 'package:constitution/ui/components/vote_index.dart';
import 'package:constitution/ui/components/vote_result_bubble.dart';
import 'vote_result_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class VoteResultView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<VoteResultViewModel>.reactive(
      builder: (context, model, child) => Container(
        color: Colors.white,
        child: SafeArea(
          bottom: true,
          top: false,
          child: Scaffold(
            backgroundColor: Colors.white,
            key: GlobalKey<ScaffoldState>(),
            appBar: AppBar(
              title: Text("Результат"),
              actions: [
                FlatButton.icon(
                  textColor: red,
                  icon: Icon(Icons.share),
                  label: Text('Поделиться'),
                  onPressed: () => model.share(context),
                )
              ],
            ),
            body: Container(
              width: screenWidth(context),
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.only(bottom: 32),
                      child: VoteResultBubble(result: model.result)),
                  Column(
                    children: [
                      Text('Идекс вашего отношения:'),
                      VoteIndex(
                        index: model.resultIndex,
                      )
                    ],
                  ),
                  if (model.connectionError != null) Container(
                    padding: EdgeInsets.only(top: 32),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Icon(Icons.error, color: red,),
                        ),
                        Expanded(child: Text(model.connectionError, style: TextStyle(color: red, fontSize: 15),)),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: FlatButton.icon(onPressed: model.securityInfo, icon: Icon(Icons.info_outline), label: Text('О безопасности')),
                  )
                ],
              ),
            ),
            floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
            floatingActionButton: model.isBusy
                ? null
                : model.data
                    ? FloatingActionButton.extended(
                        foregroundColor: Colors.white,
                        onPressed: () {
                          model.updateDialog();
                        },
                        label: Text('Обновить результат'))
                    : FloatingActionButton.extended(
                        onPressed: model.navigateToSignIn,
                        foregroundColor: Colors.white,
                        icon: model.isSending
                            ? SizedBox(
                                width: 20,
                                height: 20,
                                child: CircularProgressIndicator(
                                  strokeWidth: 3,
                                  valueColor:
                                      AlwaysStoppedAnimation<Color>(Colors.white),
                                ))
                            : Icon(Icons.send),
                        label: Text('Анонимно опубликовать')),
          ),
        ),
      ),
      viewModelBuilder: () => VoteResultViewModel(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'info_viewmodel.dart';

class InfoView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<InfoViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Информация'),
        ),
        body: Center(
//          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Container(
                  padding: EdgeInsets.all(16),
                  constraints: BoxConstraints(
                    maxWidth: 500
                  ),
                  child: Text(
                      'Это приложение\u00A0– независимая гражданская инициатива. Его цель\u00A0– информировать общество о\u00A0предложенных на\u00A0голосование нововведениях в\u00A0Конституцию РФ. Приложение не\u00A0является официальным способом онлайн голосования или средством агитации.\n\n Данные о\u00A0ваших ответах не\u00A0будут отправляться с\u00A0вашего устройства без\u00A0вашего согласия.'),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Text('Автор приложения: Антон Ощепков'),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Text('Автор фото: Lucas Carl'),
              )
            ],
          ),
        ),
      ),
      viewModelBuilder: () => InfoViewModel(),
    );
  }
}

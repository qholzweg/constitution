import 'package:constitution/app/locator.dart';
import 'package:constitution/datamodels/vote_result.dart';
import 'package:constitution/theme.dart';
import 'package:constitution/ui/components/cons_pros_stats.dart';
import 'package:constitution/ui/components/dialog_service.dart';
import 'package:constitution/ui/components/vote_bubble_template.dart';
import 'package:constitution/ui/components/vote_result_bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'survey_stats_detailed_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class SurveyStatsDetailedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SurveyStatsDetailedViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 40,
            ),
            onPressed: model.navigateToHome,
          ),
          title: Text("Статистика"),
        ),
        body: model.hasError
            ? Container(
                color: red,
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Icon(
                      Icons.cloud_off,
                      size: 64,
                      color: Colors.white,
                    ),
                    Text(
                      'Мы не можем загрузить данные, проверьте, пожалуйста, ваше интернет соединение',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              )
            : Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: model.isBusy
                    ? Center(child: CircularProgressIndicator())
                    : SmartRefresher(
                        enablePullDown: true,
                        enablePullUp: false,
                        header: WaterDropHeader(
                          complete: Text("Данные обновлены"),
                          failed: Text("Не могу связаться с сервером"),
                        ),
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus mode) {
                            Widget body;
                            if (mode == LoadStatus.idle) {
                              body = Text("Потяните, чтобы обновить");
                            } else if (mode == LoadStatus.loading) {
                              body = CupertinoActivityIndicator();
                            } else if (mode == LoadStatus.failed) {
                              body = Text("Загрузка не удалась!");
                            } else if (mode == LoadStatus.canLoading) {
                              body = Text("отпустите, чтобы загрузить больше");
                            } else {
                              body = Text("Все загружено");
                            }
                            return Container(
                              height: 55.0,
                              child: Center(child: body),
                            );
                          },
                        ),
                        controller: model.refreshController,
                        onRefresh: model.onRefresh,
                        child: CustomScrollView(
                          slivers: [
                            SliverList(
                                delegate: SliverChildListDelegate([
                              Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                                    child: VoteResultBubble(
                                      crowd: true,
                                      result: model.surveyResult.getCrowdResultType(),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 32),
                                    child: ConsProsStats(
                                      pros: model.surveyResult.pros,
                                      neutrals: model.surveyResult.neutrals,
                                      cons: model.surveyResult.cons,
                                    ),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(bottom: 16),
                                      child: Text(
                                        'Как оценивают отдельные пукты:',
                                        style: TextStyle(fontSize: 20),
                                      ))
                                ],
                              ),
                            ])),
                            SliverList(
                              delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) =>
                                      VoteStatsWidget(
                                        title: model.survey.getTitle(index),
                                        vote: model.surveyResult.votes[index],
                                        votesCount: model.surveyResult.votesCount,
                                      ),
                                  childCount: model.surveyResult.votes.length),
                            )
                          ],
                        ),
                      ),
              ),
      ),
      viewModelBuilder: () => SurveyStatsDetailedViewModel(),
    );
  }
}

class VoteStatsWidget extends StatelessWidget {
  final String title;
  final VoteResult vote;
  final int votesCount;

  VoteStatsWidget({this.title, this.vote, this.votesCount});

  Future<void> weightHelpDialog () async {
    final DialogueService _dialogueService = locator<DialogueService>();
    await _dialogueService.showDialog(
        title: 'Средняя важность поправки',
        description: 'На основании оценок пользователей мы\u00A0считаем среднюю важность поправки от\u00A01 до\u00A010.',
        buttonTitle: 'Понятно'
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 32),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 8),
            child: VoteBubbleTemplate(
              title: title,
              resultType: vote.crowdResultType(),
              footer: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Важность: '),
                  Text(
                    vote.calcAverageWeight(votesCount).toStringAsFixed(1),
                    style: TextStyle(fontSize: 20),
                  ),
                  IconButton(
                    icon: Icon(Icons.help, color: Colors.black.withOpacity(.5),),
                    iconSize: 28,
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                    onPressed: weightHelpDialog,
                  )
                ],
              ),
            ),
          ),
          ConsProsStats(
            cons: vote.cons,
            neutrals: vote.neutrals,
            pros: vote.pros,
          )
        ],
      ),
    );
  }
}

import 'package:constitution/app/common_functions.dart';
import 'package:constitution/app/locator.dart';
import 'package:constitution/datamodels/survey_result.dart';
import 'package:constitution/services/survey_service.dart';
import 'package:constitution/services/store_service.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:constitution/app/router.gr.dart';


class SurveyStatsDetailedViewModel extends FutureViewModel<SurveyResult> {
  final StoreService _storeService = locator<StoreService>();
  final SurveyService survey = locator<SurveyService>();
  SurveyResult surveyResult;
  final NavigationService _navigationService = locator<NavigationService>();

  Future<SurveyResult> getDataFromServer() async {
      SurveyResult _result = await _storeService.getResultFuture();
      surveyResult = _result;
      return _result;
  }

  RefreshController refreshController = RefreshController(initialRefresh: false);

  void onRefresh() async{
    if (!await checkConnection(needDialog: false)) refreshController.refreshFailed();
    // monitor network fetch
    surveyResult = await _storeService.getSurveyResult().catchError((onError) => refreshController.refreshFailed());
    notifyListeners();
    // if failed,use refreshFailed()
    refreshController.refreshCompleted();
  }

  Future navigateToHome() async {
      await _navigationService.navigateTo(Routes.homeViewRoute);
  }

  @override
  Future<SurveyResult> futureToRun() => getDataFromServer();
}
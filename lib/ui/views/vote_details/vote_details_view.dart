import 'package:constitution/theme.dart';
import 'package:constitution/ui/views/vote_details/vote_details_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:stacked/stacked.dart';

class VoteDetailsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<VoteDetailsViewModel>.reactive(
      builder: (context, model, child) => DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text(model.title),
            bottom: TabBar(
              labelColor: blue,
              tabs: [
                Tab(
                  text: 'Будет',
                ),
                Tab(
                  text: 'Было',
                )
              ],
            ),
          ),
          body: Center(
            child: Container(
              constraints: BoxConstraints(
                maxWidth: 500
              ),
              padding: EdgeInsets.symmetric(vertical: 32, horizontal: 16),
              child: TabBarView(
                children: [
                  ListView(
                    children: [
                      if (model.description != null &&
                          model.description.isNotEmpty)
                        _buildDescription(model),
                      if (model.law != null && model.law.isNotEmpty)
                        _buildLaw(model),
                    ],
                  ),
                  _buildCurrentLaw(model),
                ],
              ),
            ),
          ),
        ),
      ),
      viewModelBuilder: () => VoteDetailsViewModel(),
    );
  }

  Widget _buildLaw(VoteDetailsViewModel model) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Icon(MdiIcons.text),
              ),
              Text(
                'Текст поправки:',
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.start,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Text(
            model.law,
            style: TextStyle(fontSize: 15, color: Colors.grey[600]),
          ),
        ),
        if (model.author != null && model.author.isNotEmpty)
          Align(
              alignment: Alignment.centerRight,
              child: Container(
                  child: Text(
                'Кто внес: ${model.author}',
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontSize: 14,
                ),
              ))),
      ],
    );
  }

  Widget _buildCurrentLaw(VoteDetailsViewModel model) {
    if (model.currentLaw != null && model.currentLaw.isNotEmpty)
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Icon(MdiIcons.text),
              ),
              Flexible(
                child: Text(
                  'Текст действующей Конституции:',
                  softWrap: true,
                  style: TextStyle(fontSize: 18),
                  textAlign: TextAlign.start,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Text(
            model.currentLaw,
            style: TextStyle(fontSize: 15, color: Colors.grey[600]),
          ),
        ),
      ],
    );
    else return Text('Такого пункта не было', style: TextStyle(fontSize: 18),);
  }

  Widget _buildDescription(VoteDetailsViewModel model) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 32.0),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 16),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Icon(Icons.help_outline),
                ),
                Text(
                  'Справка',
                  style: TextStyle(fontSize: 18),
                )
              ],
            ),
          ),
          Text(model.description),
        ],
      ),
    );
  }
}

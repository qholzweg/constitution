import 'package:constitution/app/locator.dart';
import 'package:constitution/services/survey_service.dart';
import 'package:constitution/services/votes_service.dart';
import 'package:stacked/stacked.dart';


class VoteDetailsViewModel extends BaseViewModel {
  final VotesService _votesService = locator<VotesService>();
  final SurveyService _surveyService = locator<SurveyService>();

  String get title => 'Описание';
  int get currentIndex => _votesService.currentIndex;
  String get description => _surveyService.getDescription(currentIndex);
  String get law => _surveyService.getLaw(currentIndex);
  String get currentLaw => _surveyService.getCurrentLaw(currentIndex);
  String get author => _surveyService.getAuthor(currentIndex);


}
import 'package:constitution/datamodels/message.dart';
import 'package:constitution/datamodels/vote_result.dart';
import 'package:constitution/theme.dart';
import 'package:constitution/ui/components/bubble.dart';
import 'package:constitution/ui/smart_widgets/vote_bubble/vote_bubble.dart';
import 'package:constitution/ui/views/vote/vote_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:constitution/app/common_functions.dart';

class VoteView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double _sizeMiltiplier = screenWidth(context) > 800 ? 1.3 : 1;
    return ViewModelBuilder<VoteViewModel>.reactive(
      builder: (context, model, child) => Container(
        color: Colors.white,
        child: SafeArea(
          top: false,
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(
                  Icons.chevron_left,
                  size: 40,
                ),
                onPressed: model.navigateBack,
              ),
              title: Text(model.title),
            ),
            body: Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    constraints: BoxConstraints(
                      maxWidth: 500
                    ),
                    height: screenHeightExcludingToolbar(context) - 80,
                    padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                    child: AnimatedSwitcher(
                      transitionBuilder: (Widget child, Animation<double> animation) {
                        Animation<Offset> offsetAnimation;
                        if (model.isAnimatingBack) {
                          offsetAnimation = child.key == ValueKey(model.currentIndex + 1) ?
                          Tween<Offset>(begin: Offset(1.1, 0.0), end: Offset(0, 0.0)).animate(animation)
                              : Tween<Offset>(begin: Offset(-1, 0.0), end: Offset(0, 0.0)).animate(animation);
                          if (child.key == ValueKey(model.currentIndex)) model.isAnimatingBack = false;
                        } else
                        offsetAnimation = child.key == ValueKey(model.currentIndex) ?
                        Tween<Offset>(begin: Offset(1.1, 0.0), end: Offset(0.0, 0.0)).animate(animation)
                        : Tween<Offset>(begin: Offset(-1, 0.0), end: Offset(0.0, 0.0)).animate(animation);
                        return SlideTransition(
                        child: child,
                        position: offsetAnimation,
                      );
                      },
                        duration: Duration(milliseconds: 200),
                        child: ListView(
                          key: ValueKey(model.currentIndex),
                          children: [
                            if (model.needTitleBubble) Bubble(
                              text:
                              'Поддерживаете ли вы\u00A0внесение изменений в\u00A0Конституцию РФ? Оцените отдельно самые важные поправки.',
                              type: MessageType.neutral,
                            ),
                            VoteBubble(),
                          ],
                        )
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.thumb_down,
                            color: red,
                          ),
                          iconSize: 55 * _sizeMiltiplier,
                          onPressed: () => model.vote(VoteResultType.con),
                        ),
                        RaisedButton(
                          child: Text('Все равно', style: TextStyle(fontSize: 14 * _sizeMiltiplier),),
                          onPressed: () => model.vote(VoteResultType.neutral),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20 * _sizeMiltiplier)),
                          padding:
                              EdgeInsets.symmetric(horizontal: 16 * _sizeMiltiplier, vertical: 10 * _sizeMiltiplier),
                          elevation: 0,
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.thumb_up,
                            color: green,
                          ),
                          iconSize: 55 * _sizeMiltiplier,
                          onPressed: () => model.vote(VoteResultType.pro),
                        )
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: AnimatedOpacity(
                    duration: Duration(milliseconds: 200),
                      curve: Curves.easeOut,
                      opacity: model.checkVisible ? .8 : 0,
                      onEnd: model.hideCheck,
                      child: Visibility(
                          visible: !model.isCheckedHidden,
                          child: Icon(Icons.check_circle, size: MediaQuery.of(context).size.width - 64, color: blue[100],))),
                ),
              ],
            ),
          ),
        ),
      ),
      viewModelBuilder: () => VoteViewModel(),
    );
  }
}

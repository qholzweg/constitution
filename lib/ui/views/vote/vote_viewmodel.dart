import 'package:constitution/app/locator.dart';
import 'package:constitution/app/router.gr.dart';
import 'package:constitution/datamodels/vote_result.dart';
import 'package:constitution/services/votes_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';


class VoteViewModel extends BaseViewModel {
  final _votesService = locator<VotesService>();

  String get title => 'Блок ${_votesService.currentIndex + 1} из ${_votesService.surveyResult.votes.length}';
  bool get needTitleBubble => _votesService.currentIndex == 0;
  int get currentIndex => _votesService.currentIndex;
  bool isAnimatingBack = false;
  bool checkVisible = false;
  bool isCheckedHidden = true;

  void hideCheck () {
    if (checkVisible) {
      checkVisible = false;
      Future.delayed(Duration(milliseconds: 200), () {
        isCheckedHidden = true;
        notifyListeners();
      });
      notifyListeners();
    }
  }

  final NavigationService _navigationService = locator<NavigationService>();

  void vote (VoteResultType voteResult) async {
    checkVisible = true;
    isCheckedHidden = false;
    if (_votesService.next(voteResult))
      notifyListeners();
    else await _navigationService.navigateTo(Routes.voteResultViewRoute);
  }

  Future navigateBack() async {
    if (_votesService.previous()) {
      isAnimatingBack = true;
      notifyListeners();
    } else {
      _votesService.clear();
      await _navigationService.navigateTo(Routes.homeViewRoute);
    }
  }

}
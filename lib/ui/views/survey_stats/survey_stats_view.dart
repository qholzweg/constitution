import 'package:constitution/theme.dart';
import 'package:constitution/ui/components/cons_pros_stats.dart';
import 'package:constitution/ui/components/vote_result_bubble.dart';
import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'survey_stats_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class SurveyStatsView extends StatelessWidget {
  final bool resultPublished;
  SurveyStatsView({this.resultPublished = false});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SurveyStatsViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 40,
            ),
            onPressed: model.navigateToHome,
          ),
          title: Text("Статистика"),
        ),
        body: model.hasError
            ? Container(
                color: red,
                alignment: Alignment.center,
                padding: EdgeInsets.all(32),
                child: Column(
                  children: [
                    Icon(
                      Icons.cloud_off,
                      size: 64,
                      color: Colors.white,
                    ),
                    Text(
                      'Мы не можем загрузить данные, проверьте, пожалуйста, ваше интернет соединение',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              )
            : Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: model.isBusy
                    ? Center(child: CircularProgressIndicator())
                    : SmartRefresher(
                        enablePullDown: true,
                        enablePullUp: false,
                        header: WaterDropHeader(
                          complete: Text("Данные обновлены"),
                          failed: Text('Не могу связаться с сервером'),
                        ),
                        footer: CustomFooter(
                          builder: (BuildContext context, LoadStatus mode) {
                            Widget body;
                            if (mode == LoadStatus.idle) {
                              body = Text("Потяните, чтобы обновить");
                            } else if (mode == LoadStatus.loading) {
                              body = CupertinoActivityIndicator();
                            } else if (mode == LoadStatus.failed) {
                              body = Text("Загрузка не удалась!");
                            } else if (mode == LoadStatus.canLoading) {
                              body = Text("отпустите, чтобы загрузить больше");
                            } else {
                              body = Text("Все загружено");
                            }
                            return Container(
                              height: 55.0,
                              child: Center(child: body),
                            );
                          },
                        ),
                        controller: model.refreshController,
                        onRefresh: model.onRefresh,
                        child: CustomScrollView(
                          slivers: [
                            SliverList(
                                delegate: SliverChildListDelegate([
                              Padding(
                                padding: const EdgeInsets.only(top: 24.0),
                                child: Column(
                                  children: [
                                    if (resultPublished) Padding(
                                      padding: const EdgeInsets.only(bottom: 32),
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(right: 8.0),
                                            child: Icon(Icons.error, color: green,),
                                          ),
                                          Expanded(child: Text('Ваш результат учтен.', style: TextStyle(color: green, fontSize: 15),)),
                                        ],
                                      ),
                                    ),
                                    VoteResultBubble(
                                      crowd: true,
                                      result: model.surveyResult
                                          .getCrowdResultType(),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(bottom: 32),
                                      child: ConsProsStats(
                                        pros: model.surveyResult.pros,
                                        neutrals: model.surveyResult.neutrals,
                                        cons: model.surveyResult.cons,
                                      ),
                                    ),
                                    FlatButton(
                                      child: Text('Подробная статистика'),
                                      onPressed: model.navigateToDetails,
                                    )
                                  ],
                                ),
                              ),
                            ])),
                          ],
                        ),
                      ),
              ),
      ),
      viewModelBuilder: () => SurveyStatsViewModel(),
    );
  }
}

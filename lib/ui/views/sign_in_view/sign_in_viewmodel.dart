import 'dart:io';

import 'package:constitution/app/locator.dart';
import 'package:constitution/services/auth_service.dart';
import 'package:device_info/device_info.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:constitution/app/constants.dart' as Constants;

class SignInViewModel extends FutureViewModel<bool> {
  final AuthService _authService = locator<AuthService>();
  final NavigationService _navigationService = locator<NavigationService>();
  bool signed;
  String userID;
  String failText =
      'Для публикации результатов необходимо выполнить вход. Пожалуйста, проверьте ваше интернет соединение и попробуйте еще раз.';
  String error;
  AuthResultUserId _result;
  int c = 0;

  String title = "Вход";
  String description =
      "Для предотвращения повторной отправки результатов одним человеком, пожалуйста, войдите, используя свою учетную запись на одной из платформ, перечисленных ниже. Результаты вашего опроса будут опубликованы полностью анонимно. Мы не запрашиваем и не храним ваши персональные данные, кроме Email. Осуществляя вход вы соглашаетсь с нашей ";
  String policy = "политикой в отношении обработки персональных данных";
  String policyUrl = Constants.PRIVACY_POLICY;

  Future<bool> supportsAppleSignIn() async {
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      var version = iosInfo.systemVersion;

      if (version.contains('13') == true) {
        return true;
      }
    }
    return false;
  }

  void signIn(SignInMethod method) async {
    _result = await _authService.signIn(method).catchError((onError) {
      _result = AuthResultUserId(success: false);
    });

    signed = _result?.success ?? false;
    userID = _result?.userId;
    if (_result.success)
      _navigationService.back(result: _result);
    else
      error = _result.error;
    notifyListeners();
  }

  Future<void> signOut() async => await _authService.signOut();

  @override
  Future<bool> futureToRun() => supportsAppleSignIn();
}

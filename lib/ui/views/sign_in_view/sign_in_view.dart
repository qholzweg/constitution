import 'package:constitution/app/common_functions.dart';
import 'package:constitution/services/auth_service.dart';
import 'package:constitution/theme.dart';
import 'package:constitution/ui/components/sign_in_button.dart';
import 'package:constitution/ui/views/sign_in_view/sign_in_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:url_launcher/url_launcher.dart';

class SignInView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SignInViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(model.title),
        ),
        body: model.isBusy
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Center(
              child: Container(
                  constraints: BoxConstraints(
                    maxWidth: 500
                  ),
                  padding: EdgeInsets.all(32),
                  child: ListView(
                    children: [
                      Container(
                          child: Disclaimer()),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 32.0),
                        child: Column(
                          children: [
                            if (model.data)
                              SignInButton(
                                type: SignInButtonType.Apple,
                                onPressed: () => model.signIn(SignInMethod.apple),
                              ),
                            SignInButton(
                              type: SignInButtonType.Google,
                              onPressed: () => model.signIn(SignInMethod.google),
                            ),
                            SignInButton(
                              type: SignInButtonType.Facebook,
                              onPressed: () =>
                                  model.signIn(SignInMethod.facebook),
                            ),
                          ],
                        ),
                      ),
//                    SignInButton(
//                      text: 'Выйти',
//                      icon: Icon(Icons.exit_to_app, color: Colors.white,),
//                      onPressed: model.signOut,
//                    ),
//                    SignInButton(
//                      text: 'Войти анонимно',
//                      onPressed: () => model.signIn(SignInMethod.anonymous),
//                    ),
                      Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          model.signed == null
                              ? ''
                              : (model.signed ? 'Вход выполнен' : model.failText),
                          style: TextStyle(color: Colors.red),
                        ),
                      )
                    ],
                  ),
                ),
            ),
      ),
      viewModelBuilder: () => SignInViewModel(),
    );
  }
}

class Disclaimer extends ViewModelWidget<SignInViewModel> {
  const Disclaimer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, SignInViewModel model) {
    double _sizeMultyplier = screenHeight(context) < 600 ? .8 : screenHeight(context) > 800 ? 1.2 : 1;
    return RichText(
        text: TextSpan(children: [
      TextSpan(
        text: model.description,
        style: TextStyle(
          fontSize: 16 * _sizeMultyplier,
          fontWeight: FontWeight.w500,
        ),
      ),
      TextSpan(
          text: model.policy,
          style:
              TextStyle(fontSize: 16 * _sizeMultyplier, fontWeight: FontWeight.w500, color: blue),
          recognizer: TapGestureRecognizer()
            ..onTap = () async {
              if (await canLaunch(model.policyUrl)) {
                await launch(
                  model.policyUrl,
                );
              }
            })
    ],style: Theme.of(context).textTheme.caption));
  }
}

import 'package:constitution/app/locator.dart';
import 'package:constitution/app/router.gr.dart';
import 'package:constitution/services/store_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HomeViewModel extends BaseViewModel {
  StoreService _votesStoreService = locator<StoreService>();

  String description1 = 'Это приложение помогает понять основной смысл поправок к\u00A0Конституции Российской Федерации. На\u00A0основании вашего отношения к\u00A0самым важным поправкам оно определит, как вы\u00A0относитесь к\u00A0изменениям в\u00A0целом, а\u00A0также покажет отношение других людей.';
  String start = 'НАЧАТЬ';
  String change = 'ЗАПУСТИТЬ ЗАНОВО';


  void clearResult() {
    _votesStoreService.clearResult();
  }

  final NavigationService _navigationService = locator<NavigationService>();

  Future navigateToVote() async {
    await _navigationService.navigateTo(Routes.voteViewRoute);
  }
  Future navigateToSignIn() async {
    await _navigationService.navigateTo(Routes.signInViewViewRoute);
  }
  Future navigateToInfo() async {
    await _navigationService.navigateTo(Routes.infoViewRoute);
  }

  Future navigateToStats() async {
    await _navigationService.navigateTo(Routes.surveyStatsViewRoute);
  }

}
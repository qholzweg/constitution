import 'package:constitution/app/common_functions.dart';
import 'package:constitution/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stacked/stacked.dart';
import 'home_viewmodel.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double _sizeMultyplier = screenHeight(context) < 600
        ? .8
        : screenHeight(context) > 1200 ? 1.2 : 1;
    return ViewModelBuilder<HomeViewModel>.reactive(
      builder: (context, model, child) => Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/main_bg@3x.jpg"),
                fit: BoxFit.cover)),
        child: AnnotatedRegion(
          value: SystemUiOverlayStyle.light,
          child: SafeArea(
            child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                backgroundColor: blue.withOpacity(0.1),
                elevation: 0,
                leading: Container(),
                title: Text(
                  'Поправки в Конституцию РФ',
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
                actions: [
                  IconButton(
                    icon: Icon(Icons.info, color: Colors.white),
                    onPressed: () => model.navigateToInfo(),
                  )
                ],
              ),
              body: Container(
                padding: EdgeInsets.all(16),
                constraints: BoxConstraints(
                  maxHeight: screenHeightExcludingToolbar(context) - 80,
                ),
                child: ListView(
                  children: [
                    Container(
                      constraints: BoxConstraints(
                        minHeight: screenHeight(context) - 200,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Image.asset(
                            'assets/images/book-open.png',
                            height: screenHeightExcludingToolbar(context) / 5,
                          ),
                          Column(
                            children: [
                              Container(
                                constraints: BoxConstraints(maxWidth: 500),
                                padding: const EdgeInsets.only(
                                    top: 20.0, bottom: 24),
                                child: Text(
                                  model.description1,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16 * _sizeMultyplier,
                                      height: 1.3,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              RaisedButton(
                                child: Text("Смотреть статистику", style: TextStyle(fontSize: 14 * _sizeMultyplier),),
                                onPressed: model.navigateToStats,
                              ),
                            ],
                          ),

//                    Column(
//                      children: [
//                        FlatButton(
//                          child: Text("Залогиниться", style: TextStyle(color: Colors.white),),
//                          onPressed: model.navigateToSignIn,
//                        ),
//                        FlatButton(
//                          child: Text("Очистить результат", style: TextStyle(color: Colors.white),),
//                          onPressed: model.clearResult,
//                        ),
//                      ],
//                    ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              floatingActionButton: FloatingActionButton.extended(
                  onPressed: () => model.navigateToVote(),
                  backgroundColor: red,
                  label: Text(
                    model.start,
                    style: TextStyle(color: Colors.white),
                  )),
            ),
          ),
        ),
      ),
      viewModelBuilder: () => HomeViewModel(),
    );
  }
}

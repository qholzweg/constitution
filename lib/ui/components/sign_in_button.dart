import 'package:constitution/theme.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

/// A type for the authorization button.
enum SignInButtonType { defaultButton, Apple, Google, Facebook, VK }

/// A button for Sign in With Apple
class SignInButton extends StatefulWidget {
  /// The callback that is called when the button is tapped or otherwise activated.
  ///
  /// If this is set to null, the button will be disabled.
  final VoidCallback onPressed;

  /// A type for the authorization button.
  final SignInButtonType type;

  /// A style for the authorization button.

  /// A custom corner radius to be used by this button.
  final double cornerRadius;

  final String text;

  final Icon icon;

  const SignInButton(
      {this.onPressed,
      this.type = SignInButtonType.defaultButton,
      this.cornerRadius = 22,
      this.text,
      this.icon})
      : assert(type != null),
        assert(cornerRadius != null);

  @override
  State<StatefulWidget> createState() => _SignInButtonState();
}

class _SignInButtonState extends State<SignInButton> {
  bool _isTapDown = false;

  @override
  Widget build(BuildContext context) {
    final bgColor = blue;
    final textColor = Colors.white;
    final text =
        widget.text ?? 'Войти с ${widget.type.toString().split('.').last}';
    final Icon icon = widget.icon != null ? widget.icon : widget.type == SignInButtonType.Apple
        ? Icon(MdiIcons.apple, color: textColor)
        : widget.type == SignInButtonType.Facebook
            ? Icon(MdiIcons.facebook, color: textColor)
            : widget.type == SignInButtonType.Google
                ? Icon(MdiIcons.google, color: textColor)
                : widget.type == SignInButtonType.VK
                    ? Icon(MdiIcons.vk, color: textColor)
                    : null;
    final double iconBottomMargin =
        widget.type == SignInButtonType.Apple ? 3 : 0;
    final double textLeftMargin = icon != null ? 16 : 42;

    return GestureDetector(
      onTapDown: (_) => setState(() => _isTapDown = true),
      onTapUp: (_) {
        setState(() => _isTapDown = false);
        widget?.onPressed();
      },
      onTapCancel: () => setState(() => _isTapDown = false),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 100),
        constraints: BoxConstraints(
          minHeight: 32,
          maxHeight: 64,
          minWidth: 200,
        ),
        height: 44,
        margin: EdgeInsets.only(bottom: 16),
        decoration: BoxDecoration(
          color: _isTapDown ? Colors.grey : bgColor,
          borderRadius: BorderRadius.all(
            Radius.circular(widget.cornerRadius),
          ),
        ),
        child: Center(
            child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  bottom: iconBottomMargin, left: 16, right: textLeftMargin),
              child: SizedBox(
                child: icon,
              ),
            ),
            Text(
              text,
              style: TextStyle(
                fontSize: 19,
                fontWeight: FontWeight.w500,
                letterSpacing: .3,
                wordSpacing: -.5,
                color: textColor,
              ),
            ),
          ],
        )),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import 'package:constitution/theme.dart';


class VoteIndex extends StatelessWidget {
  final index;
  VoteIndex({this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: 500
      ),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Row(
            children: [
              LinearPercentIndicator(
                width: constraints.maxWidth/2 - 1,
                padding: EdgeInsets.all(0),
                lineHeight: 24.0,
                animation: true,
                animationDuration: 1000,
                isRTL: true,
                percent: index < 0 ? index.abs() : 0,
                center: index < 0 ? Text((index * 100).toInt().toString() + '%', style: TextStyle(color: Colors.white, fontSize: 18),) : null,
                linearStrokeCap: LinearStrokeCap.butt,
                backgroundColor: Colors.grey,
                progressColor: red,
              ),
              Container(
                width: 2,
                height: 40,
                color: Colors.grey[700],
              ),
              LinearPercentIndicator(
                padding: EdgeInsets.all(0),
                width: constraints.maxWidth/2 - 1,
                lineHeight: 24.0,
                animation: true,
                animationDuration: 1000,
                percent: index > 0 ? index : 0,
                center: index > 0 ? Text((index * 100).toInt().toString() + '%', style: TextStyle(color: Colors.white, fontSize: 18)) : null,
                linearStrokeCap: LinearStrokeCap.butt,
                backgroundColor: Colors.grey,
                progressColor: green,
              ),
            ],
          );
        }
      ),
    );
  }
}
import 'package:constitution/datamodels/vote_result.dart';
import 'package:constitution/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class VoteResultBubble extends StatelessWidget {
  final VoteResultType result;
  final bool crowd;

  VoteResultBubble({this.result, this.crowd = false});

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints:
            BoxConstraints(maxWidth:500),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        margin: EdgeInsets.only(bottom: 16),
        decoration: BoxDecoration(
            color: getColor(result),
            borderRadius: BorderRadius.all(
              const Radius.circular(25.0),
            )),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    'Поддерживаете ли\u00A0вы внесение изменений в\u00A0Конституцию РФ?',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
                Icon(
                  getIcon(result),
                  size: 48,
                  color: Colors.white,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Text(
                '${crowd ? 'Большинство ' : ''}${getText(result)}',
                style: TextStyle(color: Colors.white, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ));
  }

  String getText(VoteResultType result) => result == VoteResultType.pro
      ? "ЗА"
      : result == VoteResultType.con ? "ПРОТИВ" : "НЕЙТРАЛЬНО";

  IconData getIcon(VoteResultType result) => result == VoteResultType.pro
      ? Icons.thumb_up
      : result == VoteResultType.con ? Icons.thumb_down : Icons.pan_tool;

  ///green if pro, red if con, blue if neutral
  MaterialColor getColor(VoteResultType result) => result == VoteResultType.pro
      ? green
      : result == VoteResultType.con ? red : blue;
}

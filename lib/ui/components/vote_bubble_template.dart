import 'package:constitution/datamodels/vote_result.dart';
import 'package:constitution/theme.dart';
import 'package:flutter/material.dart';

class VoteBubbleTemplate extends StatelessWidget {
  final Function onDetailsPress;
  final String title;
  final String description;
  final String law;
  final String author;
  final Widget footer;
  final VoteResultType resultType;
  VoteBubbleTemplate({this.title, this.description, this.law, this.author, this.onDetailsPress, this.footer, this.resultType = VoteResultType.neutral});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: 500
      ),
      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: getColor(resultType),
                    borderRadius: BorderRadius.all(
                      const Radius.circular(25.0),
                    ),
                    boxShadow: [
                      new BoxShadow(
                        color: blue[200],
                        offset: new Offset(0.0, 25.0),
                      )
                    ],
                  ),
                  child: onDetailsPress != null
                      ? Column(
                    children: <Widget>[
                      _buildContent(title),
                      Container(
                        width: 180,
                        padding: EdgeInsets.only(bottom: 8),
                        child: OutlineButton(
                          borderSide: BorderSide(color: Colors.white),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Icon(
                                  Icons.info,
                                  color: Colors.white,
                                ),
                              ),
                              Text('Подробнее', style: TextStyle(color: Colors.white),)
                            ],
                          ),
                          onPressed: () => onDetailsPress(),
                        ),
                      ),
                    ],
                  )
                      : _buildContent(title),
                ),
              ),
            ],
          ),
          if (footer != null) Row(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  decoration: BoxDecoration(
                    color: blue[200],
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(15),
                    ),
                  ),
                  child: footer,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  ///green if pro, red if con, blue if neutral
  MaterialColor getColor(VoteResultType result) =>
      result == VoteResultType.pro ? green : result == VoteResultType.con ? red : blue;

  Widget _buildContent(text) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 16, 8, 16),
      child: Text(
        text,
        softWrap: true,
        style: TextStyle(color: Colors.white, fontSize: 18),
      ),
    );
  }
}

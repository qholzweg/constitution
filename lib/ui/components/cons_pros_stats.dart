import 'package:constitution/theme.dart';
import 'package:flutter/material.dart';

class ConsProsStats extends StatelessWidget {
  final int cons, neutrals, pros;

  ConsProsStats({this.pros, this.neutrals, this.cons});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: 500
      ),
      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 16),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: [
                Icon(
                  Icons.thumb_down,
                  color: red,
                  size: 55,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text('$cons против'),
                )
              ],
            ),
            Column(
              children: [
                Icon(
                  Icons.pan_tool,
                  color: blue,
                  size: 55,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text('$neutrals нейтральны'),
                )
              ],
            ),
            Column(
              children: [
                Icon(
                  Icons.thumb_up,
                  color: green,
                  size: 55,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text('$pros за'),
                )
              ],
            ),
          ]),
    );
  }
}
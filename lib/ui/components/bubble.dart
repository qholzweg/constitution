import 'package:constitution/datamodels/message.dart';
import 'package:constitution/theme.dart';
import 'package:flutter/material.dart';

class Bubble extends StatelessWidget {
  final MessageType type;
  final String text;

  Bubble({this.text, this.type = MessageType.system});

  Color getColor (type) {
    switch (type) {
      case MessageType.system:
        return blue;
        break;
      case MessageType.neutral:
        return Colors.grey[500];
        break;
      case MessageType.choice:
        return yellow;
        break;
      default:
        return blue;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxWidth: 500
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
//      crossAxisAlignment:  CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                margin: EdgeInsets.only(bottom: 16),
                decoration: BoxDecoration(
                    color: getColor(type),
                    borderRadius: BorderRadius.all(
                      const Radius.circular(25.0),
                    )
                ),
                child: Text(text, style: TextStyle(color: Colors.white, fontSize: 18),)
            ),
          ),
        ],
      ),
    );
  }
}

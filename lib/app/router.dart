import 'package:auto_route/auto_route_annotations.dart';
import 'package:constitution/ui/views/home/home_view.dart';
import 'package:constitution/ui/views/info/info_view.dart';
import 'package:constitution/ui/views/sign_in_view/sign_in_view.dart';
import 'package:constitution/ui/views/survey_stats/survey_stats_view.dart';
import 'package:constitution/ui/views/survey_stats_detailed/survey_stats_detailed_view.dart';
import 'package:constitution/ui/views/vote/vote_view.dart';
import 'package:constitution/ui/views/voteResult/vote_result_view.dart';
import 'package:constitution/ui/views/vote_details/vote_details_view.dart';

@MaterialAutoRouter()
class $Router {
  @initial
  HomeView homeViewRoute;

  VoteView voteViewRoute;
  VoteDetailsView voteDetailsViewRoute;
  VoteResultView voteResultViewRoute;
  SurveyStatsView surveyStatsViewRoute;
  SignInView signInViewViewRoute;
  SurveyStatsDetailedView surveyStatsDetailedViewRoute;
  InfoView infoViewRoute;
}
// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:constitution/services/api.dart';
import 'package:constitution/services/auth_service.dart';
import 'package:constitution/ui/components/dialog_service.dart';
import 'package:constitution/services/third_party_services.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:constitution/services/store_service.dart';
import 'package:constitution/services/survey_service.dart';
import 'package:constitution/services/votes_service.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  final thirdPartyServicesModule = _$ThirdPartyServicesModule();
  g.registerFactory<Api>(() => Api(g<String>()));
  g.registerLazySingleton<AuthService>(() => AuthService());
  g.registerLazySingleton<DialogueService>(() => DialogueService());
  g.registerLazySingleton<NavigationService>(
      () => thirdPartyServicesModule.navigationService);
  g.registerLazySingleton<StoreService>(() => StoreService());
  g.registerLazySingleton<SurveyService>(() => SurveyService());
  g.registerLazySingleton<VotesService>(() => VotesService());
}

class _$ThirdPartyServicesModule extends ThirdPartyServicesModule {
  @override
  NavigationService get navigationService => NavigationService();
}

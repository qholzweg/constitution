import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:constitution/ui/components/dialog_service.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';

import 'locator.dart';

String nameWithSpace(name) {
  return name != '' ? name + ' ' : '';
}

/// this gives 01:00:00 if duration more than hour or 01:00 otherwise
String dateToStr(duration) {
  String _hours = duration.inHours > 0 ? '${duration.inHours}:' : '';
  return '$_hours'
      '${(duration.inMinutes % 60).toString().padLeft(2, '0')}:'
      '${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
}

/// this gives 01, 01:00, 01:00:00 depending on duration
String dateToTwoDigits(Duration duration) {
  String _hours = duration.inHours > 0 ? '${duration.inHours}:' : '';
  String _minutes = duration.inMinutes > 0
      ? '${(duration.inMinutes % 60).toString().padLeft(2, '0')}:'
      : '';
  String _seconds = duration.inSeconds > 0
      ? '${(duration.inSeconds % 60).toString().padLeft(2, '0')}'
      : '';
  return _hours + _minutes + _seconds;
}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context,
    {double dividedBy = 1, double reducedBy = 0.0}) {
  return (screenSize(context).height - reducedBy) / dividedBy;
}

double screenWidth(BuildContext context,
    {double dividedBy = 1, double reducedBy = 0.0}) {
  return (screenSize(context).width - reducedBy) / dividedBy;
}

double screenHeightExcludingToolbar(BuildContext context,
    {double dividedBy = 1}) {
  return screenHeight(context,
      dividedBy: dividedBy,
      reducedBy:
          MediaQuery.of(context).padding.top + AppBar().preferredSize.height);
}

/// limit string by length and leaves the last word uncut at the end
String limitWords(String str, int maxLength, {separator = ' ', ending = '…'}) {
  if (str.length <= maxLength) return str;
  return str.substring(0, str.lastIndexOf(separator, maxLength)) + ending;
}

Future noConnectionDialog () async {
  DialogueService _dialogueService = locator<DialogueService>();
  await _dialogueService.showDialog(
    title: 'Проверьте соединение',
    description: 'Я не могу связаться с сервером. Проверьте интернет соединение и попробуйте повторить позднее.',
    buttonTitle: 'Хорошо',
  );
}

Future<bool> checkConnection ({bool needDialog = true}) async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.none) {
    if (needDialog) noConnectionDialog();
    return false;
  } else return true;
}

Future<bool> isOldIos () async {
  if (Platform.isIOS) {
    var iosInfo = await DeviceInfoPlugin().iosInfo;
    var version = iosInfo.systemVersion;

    if (version.contains('10') == true || version.contains('11') == true) {
      return true;
    }

  }
  return false;
}


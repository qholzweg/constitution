// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:constitution/ui/views/home/home_view.dart';
import 'package:constitution/ui/views/vote/vote_view.dart';
import 'package:constitution/ui/views/vote_details/vote_details_view.dart';
import 'package:constitution/ui/views/voteResult/vote_result_view.dart';
import 'package:constitution/ui/views/survey_stats/survey_stats_view.dart';
import 'package:constitution/ui/views/sign_in_view/sign_in_view.dart';
import 'package:constitution/ui/views/survey_stats_detailed/survey_stats_detailed_view.dart';
import 'package:constitution/ui/views/info/info_view.dart';

abstract class Routes {
  static const homeViewRoute = '/';
  static const voteViewRoute = '/vote-view-route';
  static const voteDetailsViewRoute = '/vote-details-view-route';
  static const voteResultViewRoute = '/vote-result-view-route';
  static const surveyStatsViewRoute = '/survey-stats-view-route';
  static const signInViewViewRoute = '/sign-in-view-view-route';
  static const surveyStatsDetailedViewRoute =
      '/survey-stats-detailed-view-route';
  static const infoViewRoute = '/info-view-route';
  static const all = {
    homeViewRoute,
    voteViewRoute,
    voteDetailsViewRoute,
    voteResultViewRoute,
    surveyStatsViewRoute,
    signInViewViewRoute,
    surveyStatsDetailedViewRoute,
    infoViewRoute,
  };
}

class Router extends RouterBase {
  @override
  Set<String> get allRoutes => Routes.all;

  @Deprecated('call ExtendedNavigator.ofRouter<Router>() directly')
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.homeViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => HomeView(),
          settings: settings,
        );
      case Routes.voteViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => VoteView(),
          settings: settings,
        );
      case Routes.voteDetailsViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => VoteDetailsView(),
          settings: settings,
        );
      case Routes.voteResultViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => VoteResultView(),
          settings: settings,
        );
      case Routes.surveyStatsViewRoute:
        if (hasInvalidArgs<SurveyStatsViewArguments>(args)) {
          return misTypedArgsRoute<SurveyStatsViewArguments>(args);
        }
        final typedArgs =
            args as SurveyStatsViewArguments ?? SurveyStatsViewArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) =>
              SurveyStatsView(resultPublished: typedArgs.resultPublished),
          settings: settings,
        );
      case Routes.signInViewViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => SignInView(),
          settings: settings,
        );
      case Routes.surveyStatsDetailedViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => SurveyStatsDetailedView(),
          settings: settings,
        );
      case Routes.infoViewRoute:
        return MaterialPageRoute<dynamic>(
          builder: (context) => InfoView(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

// *************************************************************************
// Arguments holder classes
// **************************************************************************

//SurveyStatsView arguments holder class
class SurveyStatsViewArguments {
  final bool resultPublished;
  SurveyStatsViewArguments({this.resultPublished = false});
}

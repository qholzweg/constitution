library constants;

const String SUPPORT_EMAIL = "qholzweg@gmail.com";
const String ANDROID_APP_ID = "com.holzweg.constitution";
const String IOS_APP_ID = "585027354";
const String LINK_TO_APP = "http://onelink.to/wjewmb";
const String PRIVACY_POLICY = "https://docs.google.com/document/d/1fYVMhqucvv5hiT2_eQU2oEeoIZP2nY4oYRimXbukzSw/edit?usp=sharing";

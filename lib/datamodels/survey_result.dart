import 'package:constitution/app/locator.dart';
import 'package:constitution/datamodels/vote_result.dart';
import 'package:constitution/services/survey_service.dart';

class SurveyResult {
  int surveyId, cons, pros, neutrals, votesCount;
  List<VoteResult> votes;

  SurveyResult(
      {this.surveyId = 0,
      this.cons = 0,
      this.neutrals = 0,
      this.pros = 0,
      this.votesCount = 0,
      this.votes});

  SurveyResult.populated(
      {this.surveyId = 0,
      this.cons = 0,
      this.neutrals = 0,
      this.pros = 0,
      this.votesCount = 0,
      this.votes}) {
    votes = populate();
  }

  SurveyResult.empty(
      {this.surveyId = 0,
      this.cons = 0,
      this.neutrals = 0,
      this.pros = 0,
      this.votesCount = 0,
      this.votes}) {
    votes = populate(empty: true);
  }

  int get voteCount => votes.length;

  static List<VoteResult> populate({bool empty = false}) {
    SurveyService _surveyService = locator<SurveyService>();
    return List.generate(_surveyService.survey.length,
        (index) => empty ? VoteResult.empty() : VoteResult());
  }

  VoteResultType voteResult(int index) => votes[index].singleToResultType();

  int voteWeight(int index) => votes[index].weight;

  double votesSum(VoteResultType resultType) {
    double sum = 0;
    votes
        .where((element) => element.singleToResultType() == resultType)
        .forEach((element) {
      sum += element.weight / 10;
    });
    return sum;
  }

  VoteResultType getMyResultType() {
    VoteResultType _result;
    double prosSum = votesSum(VoteResultType.pro);
    double conSum = votesSum(VoteResultType.con);
    if (prosSum > conSum) {
      _result = VoteResultType.pro;
    } else if ((prosSum < conSum)) {
      _result = VoteResultType.con;
    } else {
      _result = VoteResultType.neutral;
    }
    updateResult(_result);
    return _result;
  }

  double getMyIndex() {
    double prosSum = votesSum(VoteResultType.pro);
    double consSum = votesSum(VoteResultType.con);
    double sum = prosSum + consSum;
    double diff = prosSum - sum / 2;
    return diff / (sum / 2);
  }

  VoteResultType getCrowdResultType() {
    VoteResultType _result;
    if (neutrals > pros && neutrals > cons) {
      _result = VoteResultType.neutral;
    }
    else if (pros > cons) {
      _result = VoteResultType.pro;
    } else if (pros < cons) {
      _result = VoteResultType.con;
    } else {
      _result = VoteResultType.neutral;
    }
    return _result;
  }


  void updateResult(VoteResultType result) {
    cons = result == VoteResultType.con ? 1 : 0;
    pros = result == VoteResultType.pro ? 1 : 0;
    neutrals = result == VoteResultType.neutral ? 1 : 0;
  }

  SurveyResult operator +(SurveyResult other) => SurveyResult(
      cons: cons + other.cons,
      neutrals: neutrals + other.neutrals,
      pros: pros + other.pros,
      votesCount: votesCount + 1,
      votes: sumVotes(other.votes));

  SurveyResult operator -(SurveyResult other) => SurveyResult(
      cons: cons - other.cons,
      neutrals: neutrals - other.neutrals,
      pros: pros - other.pros,
      votesCount: votesCount - 1,
      votes: sumVotes(other.votes, plus: false));

  List<VoteResult> sumVotes(List<VoteResult> otherVotes, {bool plus = true}) {
    List<VoteResult> _sum = List();
    for (int i = 0; i < votes.length; i++) {
      if (plus) _sum.add(this.votes[i] + otherVotes[i]);
      else _sum.add(this.votes[i] - otherVotes[i]);
    }
    return _sum;
  }

  SurveyResult.fromMap(Map<String, dynamic> map)
      : surveyId = map['survey_id'],
        pros = map['pros'],
        cons = map['cons'],
        neutrals = map['neutrals'],
        votesCount = map['votes_count'],
        votes = votesFromJson(map['votes']);

  static List<VoteResult> votesFromJson(List<dynamic> json) {
    List<VoteResult> _votes = List();
    json.forEach((value) => _votes.add(VoteResult.fromMap(value)));
    return _votes;
  }

  Map<String, dynamic> toMap() {
    return {
      'survey_id': 0,
      'pros': pros,
      'cons': cons,
      'neutrals': neutrals,
      'votes_count': votesCount,
      'votes': votes.map((e) => e.toMap()).toList()
    };
  }
}

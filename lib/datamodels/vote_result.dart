enum VoteResultType { con, pro, neutral }

class VoteResult {
  int cons,
  pros,
  neutrals,
  weight;

  VoteResult({this.cons = 0, this.neutrals = 0, this.pros = 0, this.weight = 5});
  VoteResult.empty({this.cons = 0, this.neutrals = 0, this.pros = 0, this.weight = 0});
  
  VoteResultType singleToResultType() {
    if (cons == 1 && pros ==0 && neutrals == 0) return VoteResultType.con;
    else if (cons == 0 && pros ==1 && neutrals == 0) return VoteResultType.pro;
    else if (cons == 0 && pros ==0 && neutrals == 1) return VoteResultType.neutral;
    else return null;
  }

  VoteResultType crowdResultType() {
    VoteResultType _result;
    if (neutrals > pros && neutrals > cons) {
      _result = VoteResultType.neutral;
    }
    else if (pros > cons) {
      _result = VoteResultType.pro;
    } else if (pros < cons) {
      _result = VoteResultType.con;
    } else {
      _result = VoteResultType.neutral;
    }
    return _result;
  }

  void setValuesFromResultType({VoteResultType result, int weightValue}) {
    cons = result == VoteResultType.con ? 1 : 0;
    pros = result == VoteResultType.pro ? 1 : 0;
    neutrals = result == VoteResultType.neutral ? 1 : 0;
    weight = weightValue ?? weight;
  }

  double calcAverageWeight(int votesCount) => weight / votesCount;

  void setValues({int consValue, int neutralsValue, int prosValue, int weightValue}) {
    cons = consValue ?? cons;
    neutrals = neutralsValue ?? neutrals;
    pros = prosValue ?? pros;
    weight = weightValue ?? weight;
  }

  VoteResult operator +(VoteResult other) => VoteResult(
    cons: cons + other.cons,
    neutrals: neutrals + other.neutrals,
    pros: pros + other.pros,
    weight: weight + other.weight,
  );
  VoteResult operator -(VoteResult other) => VoteResult(
    cons: cons - other.cons,
    neutrals: neutrals - other.neutrals,
    pros: pros - other.pros,
    weight: weight - other.weight,
  );

  factory VoteResult.fromMap(Map<String, dynamic> map) => VoteResult(
      cons: map['cons'],
      neutrals: map['neutrals'],
      pros: map['pros'],
      weight: map['weight']
  );

  Map<String, dynamic> toMap() => {
    'pros': pros,
    'cons': cons,
    'neutrals': neutrals,
    'weight': weight};
}
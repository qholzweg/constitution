import 'package:constitution/datamodels/vote_result.dart';

class Vote {
  VoteResultType voteResult;
  int weight;

  Vote({this.voteResult, this.weight = 5});
  Vote.empty({this.voteResult, this.weight = 0});

  factory Vote.fromMap(Map<String, dynamic> map) => Vote(
      voteResult: VoteResultType.pro,
      weight: map['weight']
  );

  void setValues({VoteResultType result, int weightValue}) {
    voteResult = result ?? voteResult;
    weight = weightValue ?? weight;
  }



  Map<String, dynamic> toMap() => {
    'pros': voteResult == VoteResultType.pro ? 1 : 0,
    'cons': voteResult == VoteResultType.con ? 1 : 0,
    'neutrals': voteResult == VoteResultType.neutral ? 1 : 0,
    'weight': weight};
}
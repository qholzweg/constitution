enum MessageType {
  choice,
  system,
  neutral
}

class Message {
  final MessageType type;
  final String text;
  Message({this.type, this.text});
}
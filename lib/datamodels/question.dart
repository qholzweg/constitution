class Question {
  String title;
  String description;
  String law;
  String currentLaw;
  String author;
  Question({this.title, this.description, this.law});

  Question.fromMap(Map<String, dynamic> map) {
    title = map['title'];
    description = map['description'];
    law = map['law'];
    currentLaw = map['law_current'];
    author = map['author'];
  }
}